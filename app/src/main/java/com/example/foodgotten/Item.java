package com.example.foodgotten;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;


public class Item extends AsyncTask<ImageView, Void, ImageView> implements Parcelable{

    private String title;
    private String description;
    private String photoPath;
    private float rating;
    private Bitmap photo;
    private long creationtime;
    private String date;
    private ArrayList<String> folders;
    private double price = 0;
    public static final int height = 300;
    // vertical Margin
    public static final int vMargin = 15;

    public Item(String title, String description, String photoPath, float rating){
        this.title = title;
        this.description = description;
        this.photoPath = photoPath;
        this.rating = rating;
        Date currentTime = Calendar.getInstance().getTime();
        this.creationtime = currentTime.getTime();
        this.date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        this.folders = new ArrayList<>();
    }

    public Item(String title, String description, String photoPath, float rating,
                long creationtime, String date, ArrayList<String> folders){
        this(title, description, photoPath, rating);
        this.creationtime = creationtime;
        this.date = date;
        this.folders = folders;
    }

    protected Item(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.photoPath = in.readString();
        this.rating = in.readFloat();
        this.creationtime = in.readLong();
        this.date = in.readString();
        this.folders = in.readArrayList(null);
        this.price = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(photoPath);
        parcel.writeFloat(rating);
        parcel.writeLong(creationtime);
        parcel.writeString(date);
        parcel.writeList(folders);
        parcel.writeDouble(price);
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public void addItemToContext(final ScrollingActivity context){
        LinearLayout item_list = context.findViewById(R.id.item_list);

        LinearLayout itemLayout = createItemLayout(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, Item.height);
        LinearLayout textLayout = new LinearLayout(context);
        textLayout.setLayoutParams(layoutParams);
        textLayout.setOrientation(LinearLayout.VERTICAL);


        layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);
        TextView descriptionView = new TextView(context);
        descriptionView.setLayoutParams(layoutParams);
        descriptionView.setText(this.description);

        addImageToLayout(itemLayout, context);
        addTitleToLayout(textLayout, context, 24);
        addRatingBarToLayout(textLayout, context, true);
        textLayout.addView(descriptionView);
        itemLayout.addView(textLayout);

        item_list.addView(itemLayout);
    }


    public void addItemToContextCompact(final ScrollingActivity context){
        LinearLayout item_list = context.findViewById(R.id.item_list);

        LinearLayout itemLayout = createItemLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10,Item.vMargin,10,Item.vMargin);
        itemLayout.setLayoutParams(layoutParams);

        addTitleToLayout(itemLayout, context, 18);
        addRatingBarToLayout(itemLayout, context, false);
        item_list.addView(itemLayout);
    }

    public Bitmap getPicFromStream(InputStream stream, BitmapFactory.Options bmOptions) {
        if(this.photo != null){
            return this.photo;
        }
        Bitmap bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);

        //Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
        this.photo = bitmap;
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public BitmapFactory.Options getbmOptions(final AppCompatActivity context){
        InputStream stream = getImageStream(context);
        int targetH = Item.height;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(stream, null, bmOptions);
         //BitmapFactory.decodeFile(photoPath, bmOptions);

         //int photoW = bmOptions.outWidth;
         int photoH = bmOptions.outHeight;

         // Determine how much to scale down the image
         //int scaleFactor = Math.max(1, Math.min(photoW/targetW, photoH/targetH));
         int scaleFactor = Math.max(1, photoH/targetH);

         // Decode the image file into a Bitmap sized to fill the View
         bmOptions.inJustDecodeBounds = false;
         bmOptions.inSampleSize = scaleFactor;
         bmOptions.inPurgeable = true;
         bmOptions.inPreferredConfig = Bitmap.Config.RGB_565;
         bmOptions.inDither = true;

        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmOptions;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public InputStream getImageStream(final AppCompatActivity context){
        InputStream stream = null;

        if(photoPath == null || photoPath.isEmpty() || photoPath.equals("null") || !Files.exists(Paths.get(photoPath))){
            try {
                stream = context.getAssets().open("duck.png");
                if (!Files.exists(Paths.get(photoPath))){
                    System.out.println("IMAGE PATH " + photoPath + " DOES NOT EXIST");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                stream = new FileInputStream(new File(photoPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return stream;
    }

    public Bitmap getPic(final AppCompatActivity context){
        InputStream imageStream = getImageStream(context);
        BitmapFactory.Options bmOptions = getbmOptions(context);
        Bitmap imageBitmap = getPicFromStream(imageStream, bmOptions);
        if(photoPath == null || photoPath.isEmpty() || photoPath.equals("null")) {
            // two times height because I want the image to be square
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, Item.height, Item.height ,true);
        }
        return imageBitmap;
    }

    public Bitmap getFullPic(final AppCompatActivity context){
        InputStream imageStream = getImageStream(context);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap fullBitmap = getPicFromStream(imageStream, bmOptions);
        return fullBitmap;
    }

    private void setRatingStarColor(Drawable drawable, int color){
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object other){
        if(this == other){
            return true;
        }
        Item otherItem = (Item) other;

        return this.title.equals(otherItem.title) && this.rating == otherItem.rating &&
                this.description.equals(otherItem.description) &&
                this.price == otherItem.price && this.creationtime == otherItem.creationtime;

    }


    public String getTitle(){
        return this.title;
    }

    public String getDescription(){
        return this.description;
    }

    public String getPhotoPath(){
        return this.photoPath;
    }

    public float getRating() {
        return this.rating;
    }

    public long getCreationtime() { return this.creationtime; }

    public String getDate() { return this.date; }

    public ArrayList<String> getFolders(){ return this.folders; }

    public double getPrice(){ return this.price; }

    public void setTitle(String title) { this.title = title; }

    public void setCreationtime(long time) { this.creationtime = time; }

    public void setDate(String date) { this.date = date; }

    public void setFolders(ArrayList<String> folders){ this.folders = folders; }

    public void setPrice(double price){ this.price = price; }

    public String toString(){
        StringBuilder content = new StringBuilder(C.TITLE + ":" + this.title + "\t" +
                C.DESCRIPTION + ":" + this.description + "\t" +
                C.PHOTOPATH + ":" + this.photoPath + "\t" +
                C.RATING + ":" + this.rating + "\t" +
                C.CREATIONTIME + ":" + this.creationtime + "\t" +
                C.DATE + ":" + this.date + "\t" +
                C.PRICE + ":" + this.price + "\t" +
                C.FOLDERS + ":");
        for(String folder: this.folders){
            content.append(folder).append(",");
        }
        return content.toString();
    }

    @Override
    protected ImageView doInBackground(ImageView... view) {
        // load the image in the getPic method and set it in onPostExecute
        ImageView imageView = view[0];
        AppCompatActivity context = (AppCompatActivity) imageView.getContext();
        Bitmap image = getPic(context);
        return imageView;
    }

    @Override
    protected void onPostExecute(ImageView imageView) {
        AppCompatActivity context = (AppCompatActivity) imageView.getContext();
        imageView.setImageBitmap(Item.this.photo);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, FullPictureActivity.class);
                myIntent.putExtra(C.ITEM, Item.this);
                //myIntent.putExtra(C.PHOTOPATH, photoPath);
                context.startActivity(myIntent);
            }
        });
    }

    public static class RatingComparator implements Comparator<Item> {

        @Override
        public int compare(Item item1, Item item2) {
            return Float.compare(item1.getRating(), item2.getRating());
        }
    }

    public static class DateComparator implements Comparator<Item> {

        @Override
        public int compare(Item item1, Item item2) {
            return Long.compare(item1.getCreationtime(), item2.getCreationtime());
        }
    }

    public void addTitleToLayout(LinearLayout layout, ScrollingActivity context, int titleSize){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);

        // Text
        TextView titleView = new TextView(context);
        titleView.setLayoutParams(layoutParams);
        titleView.setText(this.title);
        titleView.setTextSize(titleSize);
        titleView.setTextColor(Color.BLACK);

        layout.addView(titleView);
    }

    public void addImageToLayout(LinearLayout layout, final ScrollingActivity context){
        // Image
        final ImageView imageView = new ImageView(context);
        // actually width of the image
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Item.height, LinearLayout.LayoutParams.WRAP_CONTENT);
        imageView.setLayoutParams(layoutParams);
        // maybe add ConstraintLayout to make image square
        if(this.photo == null && this.getStatus() != Status.RUNNING){
            this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageView);
        }
        else{
            this.onPostExecute(imageView);
        }

        layout.addView(imageView);
    }

    public void addRatingBarToLayout(LinearLayout layout, ScrollingActivity context, boolean addDate){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 0, 0);
        LinearLayout ratingLayout = new LinearLayout(context);  // put date next to rating
        ratingLayout.setLayoutParams(layoutParams);
        ratingLayout.setOrientation(LinearLayout.HORIZONTAL);

        RatingBar ratingBar = new RatingBar(context, null, android.R.attr.ratingBarStyleSmall);
        layoutParams.setMargins(0, 0, 140, 15);
        ratingBar.setLayoutParams(layoutParams);
        ratingBar.setNumStars(5);
        ratingBar.setRating(this.rating);
        ratingBar.setIsIndicator(true);
        ratingBar.setScaleX((float) 1.5);
        ratingBar.setScaleY((float) 1.5);
        ratingBar.setPivotX(0);
        ratingBar.setPivotY(0);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        setRatingStarColor(stars.getDrawable(2), ContextCompat.getColor(context, R.color.ratingColor));
        setRatingStarColor(stars.getDrawable(1), ContextCompat.getColor(context, R.color.ratingColorBorder));
        ratingLayout.addView(ratingBar);

        if(addDate){
            // Date
            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView dateView = new TextView(context);
            dateView.setLayoutParams(layoutParams);
            dateView.setText(this.date);
            dateView.setTextSize(12);
            ratingLayout.addView(dateView);
        }
        if(this.price > 0){
            layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(20, 0, 0, 0);
            TextView priceView = new TextView(context);
            priceView.setLayoutParams(layoutParams);
            priceView.setText(String.format("%.2f", this.price) + "€");
            priceView.setTextSize(16);
            ratingLayout.addView(priceView);
        }

        // No idea why I have to set the layoutparams for ratinglayout here again
        ratingLayout.setLayoutParams(layoutParams);
        layout.addView(ratingLayout);
    }


    public LinearLayout createItemLayout(final ScrollingActivity context){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Item.height);
        layoutParams.setMargins(10, Item.vMargin,10, Item.vMargin);

        LinearLayout itemLayout = new LinearLayout(context);
        itemLayout.setLayoutParams(layoutParams);
        itemLayout.setOrientation(LinearLayout.HORIZONTAL);
        itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Item> items = context.items;
                int index = items.indexOf(Item.this);

                Intent myIntent = new Intent(context, EditItemActivity.class);
                myIntent.putParcelableArrayListExtra(C.ITEMS, items);
                myIntent.putExtra(C.INDEX, index);
                myIntent.putExtra(C.ITEM, Item.this);
                context.startActivity(myIntent);
            }
        });

        return itemLayout;
    }

}
