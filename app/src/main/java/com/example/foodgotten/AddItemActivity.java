package com.example.foodgotten;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;

public class AddItemActivity extends AppCompatActivity {

    PhotoHandler photoHandler;
    ImageButton photoButton;
    Button saveButton;
    ImageButton addFolderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_item);

        photoHandler = new PhotoHandler(this);


        photoButton = (ImageButton) findViewById(R.id.imageButton);
        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoHandler.dispatchTakePictureIntent();
            }
        });

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = getItemFromContext();

                Intent myIntent = Utils.createIntentWithItems(getIntent(), AddItemActivity.this, ScrollingActivity.class);
                ArrayList<Item> items = Utils.getItemlistFromIntent(myIntent);
                items.add(item);

                AddItemActivity.this.startActivity(myIntent);
            }
        });

        addFolderButton = (ImageButton) findViewById(R.id.folderButton);
        addFolderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = getItemFromContext();
                Intent myIntent = Utils.createIntentWithItems(getIntent(), AddItemActivity.this, FolderSelectorActivity.class);
                myIntent.putExtra(C.ITEM, item);
                AddItemActivity.this.startActivity(myIntent);
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(C.ITEM)){
            Item incomingItem = getIntent().getParcelableExtra(C.ITEM);
            addItemContentToLayout(incomingItem);
        }

    }


    public void addItemContentToLayout(Item item){
        // ADD ITEM CONTENT TO VIEWS
        ImageButton imageButton = findViewById(R.id.imageButton);
        Bitmap image = item.getPic(this);
        imageButton.setImageBitmap(image);

        EditText titleView = findViewById(R.id.item_title);
        titleView.setText(item.getTitle());

        EditText descriptionView = findViewById(R.id.description);
        descriptionView.setText(item.getDescription());

        RatingBar ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setRating(item.getRating());
        this.photoHandler.currentPhotoPath = item.getPhotoPath();

        EditText priceView = findViewById(R.id.priceView);
        priceView.setText("" + item.getPrice());
        System.out.println(item.getPrice());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE) {
            String path = photoHandler.currentPhotoPath;
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(path, bmOptions);

            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int targetH = (int) Utils.convertDpToPixel(110, this);

            // Determine how much to scale down the image
            //int scaleFactor = Math.max(1, Math.min(photoW/targetW, photoH/targetH));
            int scaleFactor = Math.max(1, photoH/targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
            this.photoButton.setImageBitmap(bitmap);
        }
    }


    public Item getItemFromContext(){
        EditText titleView = (EditText) this.findViewById(R.id.item_title);
        String title = titleView.getText().toString();
        EditText descriptionView = (EditText) this.findViewById(R.id.description);
        String description = descriptionView.getText().toString();
        String photoPath = photoHandler.currentPhotoPath;
        RatingBar ratingBar = (RatingBar) this.findViewById(R.id.ratingBar);
        float rating = ratingBar.getRating();
        EditText priceView = (EditText) this.findViewById(R.id.priceView);
        Item item = new Item(title, description, photoPath, rating);

        if(!String.valueOf(priceView.getText()).isEmpty()){
            double price = Double.parseDouble(String.valueOf(priceView.getText()));
            item.setPrice(price);
        }

        // add folder if present
        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(C.ITEM)){
            Item incoming_item = getIntent().getParcelableExtra(C.ITEM);
            item.setFolders(incoming_item.getFolders());
        }
        return item;
    }
}
