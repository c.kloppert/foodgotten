package com.example.foodgotten;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;


public class ScrollingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    ArrayList<Item> items = new ArrayList<>();
    ArrayList<String> folders = new ArrayList<>();

    public static final String FILE_NAME = "itemfile.txt";
    public static final String FOLDERS_FILE = "foldersfile.txt";
    private Menu menu;
    private String currentFolder;
    private boolean compact;
    // Save the maximum y coordinate of the scroll view when the user scrolls to know when to
    // render more items
    private int yScrollMax = -1;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ScrollingActivity.this, AddItemActivity.class);
                myIntent.putParcelableArrayListExtra(C.ITEMS, items);
                ScrollingActivity.this.startActivity(myIntent);
            }
        });

        Bundle extras = getIntent().getExtras();
        // load from file when not provided by intent (for initial startup)
        if (extras == null || ((!extras.containsKey(C.ITEMS)))){
            load();
        }
        else{
            load_items_from_intent();
            save();
        }
        this.folders = Utils.loadFolders(this);
        Item.DateComparator dateComparator = new Item.DateComparator();
        System.out.println(this.items);
        Collections.sort(this.items, Collections.reverseOrder(dateComparator));
        showItems(this.items, this.compact);
        showFolders(this.folders);

        // Request for camera runtime permission
        if(ContextCompat.checkSelfPermission(ScrollingActivity.this, Manifest.permission.CAMERA)
        != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(ScrollingActivity.this, new String[]{Manifest.permission.CAMERA}, 100);
        }


        // add navigation button
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openNavDrawer, R.string.closeNavDrawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    public void load_items_from_intent(){
        Intent incoming_intent = getIntent();
        ArrayList<Item> items = Utils.getItemlistFromIntent(incoming_intent);
        if(items != null){
            this.items = items;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void save(){
        if(this.items.isEmpty()){
            return;
        }
        ArrayList<String> item_string_list = new ArrayList<>();
        String items_as_String = "";
        for(Item item: this.items){
            item_string_list.add(item.toString());
        }
        items_as_String = TextUtils.join("\n", item_string_list);
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(items_as_String.getBytes());
            //Toast.makeText(ScrollingActivity.this, "Saved to", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
            Boolean sync_with_dropbox = preferences.getBoolean(C.SYNC_WITH_DROPBOX, false);
            if (mWifi.isConnected() && sync_with_dropbox) {
                Intent myIntent = new Intent(ScrollingActivity.this, DropboxActivity.class);
                myIntent.putExtra(C.DROPBOX_ACTION, C.UPLOAD);
                ScrollingActivity.this.startActivity(myIntent);
            }
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public void load(){
        this.items = new ArrayList<>();

        try {
            this.items = Utils.getItemsFromFile(FILE_NAME, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showItems(ArrayList<Item> items, boolean compact){
        LinearLayout item_list = (LinearLayout) findViewById(R.id.item_list);
        item_list.removeAllViews();

        for(Item item: items){
            if(compact){
                item.addItemToContextCompact(this);
            }
            else{
                item.addItemToContext(this);
            }
        }
    }


    public void showFolders(ArrayList<String> folders){
        NavigationView navView = findViewById(R.id.nav_view);
        Menu navMenu = navView.getMenu();
        for(String folder: folders){
            MenuItem i = navMenu.add(folder);
            i.setIcon(R.drawable.ic_baseline_folder_24);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        this.menu = menu;
        MenuItem uploadMenuItem = menu.findItem(R.id.settings);
        // TODO: make upload to dropbox invisible when already syncing
        //uploadMenuItem.setVisible(false); // Set visibility based on the boolean variable


        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint("Type here to search");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                onQueryTextChange(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                ArrayList<Item> fittingItems = Utils.searchItems(searchQuery, items);
                showItems(fittingItems, compact);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem option) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = option.getItemId();

        if (id == R.id.add_folder){
            enterNameAndAddFolder();
        }

        if (id == R.id.sort_rating) {
            // reset text on sort by date (in case arrows were added there)
            MenuItem dateSorting = this.menu.findItem(R.id.sort_date);
            dateSorting.setTitle(getResources().getString(R.string.sort_date));

            Item.RatingComparator ratingComparator = new Item.RatingComparator();
            // sort ascending when previously sorted descending
            if(option.getTitle().toString().endsWith("\u2191")){
                option.setTitle(getResources().getString(R.string.sort_rating) + " \u2193");  // adds arrow down
                Collections.sort(items, ratingComparator);
            }
            // sort descending by default
            else{
                option.setTitle(getResources().getString(R.string.sort_rating) + " \u2191");  // adds arrow up
                Collections.sort(items, Collections.reverseOrder(ratingComparator));
            }
            showItems(this.items, compact);
            return true;
        }
        if (id == R.id.sort_date) {
            // reset text on sort by rating (in case arrows were added there)
            MenuItem dateSorting = this.menu.findItem(R.id.sort_rating);
            dateSorting.setTitle(getResources().getString(R.string.sort_rating));

            Item.DateComparator dateComparator = new Item.DateComparator();
            // sort ascending when previously sorted descending
            if(option.getTitle().toString().endsWith("\u2191")){
                option.setTitle(getResources().getString(R.string.sort_date) + " \u2193");
                Collections.sort(items, dateComparator);
            }
            // sort descending by default (i.e. current new items on top)
            else{
                option.setTitle(getResources().getString(R.string.sort_date) + " \u2191");
                Collections.sort(items, Collections.reverseOrder(dateComparator));
            }
            showItems(this.items, compact);
            return true;
        }
        if(id == R.id.showcompact){
            this.compact = !this.compact;
            showItems(this.items, this.compact);
        }
        if(id == R.id.settings){
            Intent myIntent = new Intent(ScrollingActivity.this, SettingsActivity.class);
            ScrollingActivity.this.startActivity(myIntent);
        }
        return super.onOptionsItemSelected(option);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        if(savedInstanceState != null){
            savedInstanceState.putParcelableArrayList("items", this.items);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        if(savedInstanceState != null) {
            this.items = savedInstanceState.getParcelableArrayList("items");
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem folderItem) {
        Toast.makeText(ScrollingActivity.this, "HOLA " + folderItem.getTitle(), Toast.LENGTH_LONG).show();
        int id = folderItem.getItemId();
        ArrayList<Item> fittingItems = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);


        // First add all items starting with the search string
        for(Item item: items){
            if(item.getFolders().contains(folderItem.getTitle())){
                fittingItems.add(item);
            }
            toolbar.setTitle(folderItem.getTitle());
        }

        if (id == R.id.nav_all_items) {
            fittingItems = this.items;
            toolbar.setTitle("Foodgotten");
        }
        showItems(fittingItems, compact);

        // Close the sidemenu after selecting a folder
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navView = findViewById(R.id.nav_view);
        drawerLayout.closeDrawer(navView);

        return false;
    }

    public void enterNameAndAddFolder(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");

        // Set up the input
        final EditText input = new EditText(this);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentFolder = input.getText().toString();
                NavigationView navView = findViewById(R.id.nav_view);
                Menu navMenu = navView.getMenu();
                MenuItem i = navMenu.add(currentFolder);
                i.setIcon(R.drawable.ic_baseline_folder_24);
                if(!folders.contains(currentFolder)){
                    folders.add(currentFolder);
                }
                Utils.saveFolders(ScrollingActivity.this.folders, ScrollingActivity.this);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}