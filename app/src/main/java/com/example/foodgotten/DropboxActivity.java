package com.example.foodgotten;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class DropboxActivity extends AppCompatActivity {
    static final int REQUEST_CODE_BROWSER = 1;
    private ProgressBar progressBar;
    private TextView loadingMessage;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        loadingMessage = findViewById(R.id.loadingMessage);

        Intent intent = getIntent();
        Uri data = intent.getData();
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String saved_refresh_token = preferences.getString("refresh_token", null);
        String dropboxAction = getDropboxAction(intent);

        if (dropboxAction == null) {
            return;
        }

        if (data == null && saved_refresh_token == null) {
            this.openDropboxAuthentication(dropboxAction);
        }
        else if (data != null || saved_refresh_token != null){
            performAction(dropboxAction, data);
        }
        else {
            System.out.println("Returning because of missing dropbox action (e.g. download or upload");
            Intent myIntent = new Intent(DropboxActivity.this, ScrollingActivity.class);
            DropboxActivity.this.startActivity(myIntent);
        }

    }

    public String getDropboxAction(Intent intent) {
        String dropboxAction = intent.getStringExtra(C.DROPBOX_ACTION);

        if (dropboxAction == null) {
            SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            dropboxAction = preferences.getString(C.DROPBOX_ACTION, null);
            editor.putString(C.DROPBOX_ACTION, null);
            editor.apply();
        }
        return dropboxAction;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
//    I think data is unnecessary here, because the refresh token should always exist when calling this method
    public void performAction(String dropboxAction, Uri data) {
        if (dropboxAction.equals(C.UPLOAD)){
            loadingMessage.setText("UPLOADING...");
            upload(data);
        }
        else if (dropboxAction.equals(C.DOWNLOAD)){
            loadingMessage.setText("DOWNLOADING...");
            String bearer_token = getBearerToken(data);
            download(bearer_token);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void upload(Uri data) {
        String bearer_token = getBearerToken(data);
        uploadFile(ScrollingActivity.FILE_NAME, bearer_token, true);
        uploadFile(ScrollingActivity.FOLDERS_FILE, bearer_token, true);
        uploadMissingImages("{\"path\":\"/storage/emulated/0/Android/data/com.example.foodgotten/files/Pictures\"}", bearer_token);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getBearerToken(Uri data) {
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String saved_refresh_token = preferences.getString("refresh_token", null);
        if (saved_refresh_token != null) {
            return getBearerTokenFromRefreshToken(saved_refresh_token);
        }

        String query = data.getQuery();
        String[] queryKeyValue = query.split("=");
        if (queryKeyValue[0].equals("code")) {
            String access_token = queryKeyValue[1];
            String refresh_token = getRefreshToken(access_token);
            String bearer_token = getBearerTokenFromRefreshToken(refresh_token);
            return bearer_token;
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getRefreshToken(String access_token) {
        SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        String saved_refresh_token = preferences.getString("refresh_token", null);
        if(saved_refresh_token != null) {
            return saved_refresh_token;
        }

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            OkHttpClient client = new OkHttpClient();
            String codeVerifier = preferences.getString("code_verifier", null);

            FormBody.Builder formBodyBuilder = new FormBody.Builder()
                    .add("code", access_token)
                    .add("grant_type", "authorization_code")
                    .add("code_verifier", codeVerifier)
                    .add("redirect_uri", "foodgotten://dropboxauth");
            RequestBody requestBody = formBodyBuilder.build();
            String credentials = "szhylfeuccf7w84:q4op9n9xkgtn9ki";
            String basicAuth = "Basic " + java.util.Base64.getEncoder().encodeToString(credentials.getBytes());

            Request request = new Request.Builder()
                    .url("https://api.dropbox.com/oauth2/token")
                    .addHeader("Authorization", basicAuth)
                    .post(requestBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    JsonObject jsonResponse = JsonParser.parseString(responseBody).getAsJsonObject();

                    // save refresh token
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("refresh_token", jsonResponse.get("refresh_token").getAsString());
                    editor.apply();

                    return jsonResponse.get("refresh_token").getAsString();
                } else {
                    System.out.println("Request failed: " + response.message());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getBearerTokenFromRefreshToken(String refresh_token) {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            OkHttpClient client = new OkHttpClient();
            String url = "https://api.dropbox.com/oauth2/token";

            FormBody.Builder formBodyBuilder = new FormBody.Builder()
                    .add("grant_type", "refresh_token")
                    .add("refresh_token", refresh_token)
                    .add("client_id", "szhylfeuccf7w84");
            RequestBody requestBody = formBodyBuilder.build();

            Request request = new Request.Builder()
                    .url("https://api.dropbox.com/oauth2/token")
                    .post(requestBody)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    JsonObject jsonResponse = JsonParser.parseString(responseBody).getAsJsonObject();

                    return jsonResponse.get("access_token").getAsString();
                } else {
                    System.out.println("Request failed: " + response.message());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    @SuppressLint("StaticFieldLeak")
    public void uploadFile(String path, String bearerToken, boolean isInternalFile){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                File localFile;
                // internal files are the itemfile.txt and foldersfile.txt
                // external files are the image files
                if (isInternalFile){
                    localFile = getFileStreamPath(path);
                }
                else {
                    localFile = new File(path);
                }
                if (!localFile.exists()){
                    return null;
                }

                String url = "https://content.dropboxapi.com/2/files/upload";

                MediaType mediaType = MediaType.parse("application/octet-stream");
                RequestBody formBody = RequestBody.create(localFile, mediaType);
                Request.Builder requestBuilder = new Request.Builder()
                        .url(url)
                        .addHeader("Authorization", "Bearer " + bearerToken)
                        .addHeader("Dropbox-API-Arg", "{\"autorename\":false,\"mode\":\"overwrite\",\"mute\":false,\"path\":\"" + localFile + "\",\"strict_conflict\":false}")
                        .addHeader("Content-Type", "application/octet-stream")
                        .post(formBody);

                try {
                    System.out.println("Uploading file: " + path);
                    Response response = client.newCall(requestBuilder.build()).execute();
                    response.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("StaticFieldLeak")
    public void uploadMissingImages(String path, String bearerToken){
        ArrayList<Item> localItems = Utils.getItemsFromFile(ScrollingActivity.FILE_NAME, this);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();
                String url = "https://api.dropboxapi.com/2/files/list_folder";
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody formBody = RequestBody.create(path, mediaType);
                Request.Builder requestBuilder = new Request.Builder()
                        .url(url)
                        .addHeader("Authorization", "Bearer " + bearerToken)
                        .addHeader("Content-Type", "application/json")
                        .post(formBody);
                ArrayList<String> localImages = new ArrayList<>();
                for (Item item: localItems) {
                    localImages.add(item.getPhotoPath());
                }
                try {
                    Response response = client.newCall(requestBuilder.build()).execute();
                    if (response.isSuccessful()) {
                        String responseBody = response.body().string();
                        JsonObject jsonResponse = JsonParser.parseString(responseBody).getAsJsonObject();
                        ArrayList<String> remoteImages = new ArrayList<>();
                        for (JsonElement image: jsonResponse.getAsJsonArray("entries")) {
                            remoteImages.add(image.getAsJsonObject().get("path_display").getAsString());
                        }
                        localImages.removeAll(remoteImages);
                    } else {
                        System.out.println("Unable to fetch images folder: " + response);
                    }
                    for(String path: localImages){
                        uploadFile(path, bearerToken, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }


                @Override
                protected void onPostExecute(Void aVoid) {
                    Intent myIntent = new Intent(DropboxActivity.this, ScrollingActivity.class);
                    DropboxActivity.this.startActivity(myIntent);
                }

        }.execute();
    }


    public void download(String bearerToken){
        downloadFile(bearerToken, C.ITEMFILE_PATH);
        downloadFile(bearerToken, C.FOLDERSFILE_PATH);
    }

//    downloads and saves the file from dropbox
    @SuppressLint("StaticFieldLeak")
    public void downloadFile(String bearerToken, String path){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                // Create an OkHttpClient instance
                OkHttpClient client = new OkHttpClient();

                // Build the URL for the GET request
                String url = "https://content.dropboxapi.com/2/files/download";

                // Prepare the request body
                RequestBody formBody = RequestBody.create(null, new byte[0]);

                // Prepare the request headers
                Request.Builder requestBuilder = new Request.Builder()
                        .url(url)
                        .addHeader("Authorization", "Bearer " + bearerToken)
                        .addHeader("Dropbox-API-Arg", "{\"path\":\"" + path + "\"}")
                        .post(formBody);

                // Perform the POST request
                try {
                    Response response = client.newCall(requestBuilder.build()).execute();
                    if (response.isSuccessful()) {
                        String responseBody = response.body().string();
                        FileOutputStream fos = null;

//                        save the file
                        try {
                            String[] pathSegments = path.split("/");
                            String filename = pathSegments[pathSegments.length-1];

                            fos = openFileOutput(filename, MODE_PRIVATE);
                            fos.write(responseBody.getBytes());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finally {
                            if(fos != null){
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        System.out.println("Error: " + response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected void onPostExecute(Void _) {
                if (path.equals(C.ITEMFILE_PATH)) {
                    try {
                        OkHttpClient client = new OkHttpClient();
                        String url = "https://content.dropboxapi.com/2/files/download_zip";

                        // Prepare the request body
                        RequestBody formBody = RequestBody.create(null, new byte[0]);

                        // Prepare the request headers
                        Request.Builder requestBuilder = new Request.Builder()
                                .url(url)
                                .addHeader("Authorization", "Bearer " + bearerToken)
                                .addHeader("Dropbox-API-Arg", "{\"path\":\"/storage/emulated/0/Android/data/com.example.foodgotten/files/Pictures\"}")
                                .post(formBody);


                        try {
                            Response response = client.newCall(requestBuilder.build()).execute();
                            if (response.isSuccessful()) {
                                // Read the response body as InputStream
                                InputStream inputStream = response.body().byteStream();

                                saveInputStreamToFile(inputStream, "downloaded_images.zip");
                                unzip("downloaded_images.zip"); // Provide the destination to extract files

                                Intent myIntent = new Intent(DropboxActivity.this, ScrollingActivity.class);
                                DropboxActivity.this.startActivity(myIntent);
                            } else {
                                System.out.println("Error: " + response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.execute();
    }

    // Method to save InputStream to a local file
    private void saveInputStreamToFile(InputStream inputStream, String filePath) {
        FileOutputStream fos = null;

        try{
            BufferedInputStream in = new BufferedInputStream(inputStream);
            fos = openFileOutput(filePath, MODE_PRIVATE);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void unzip(String zipFilePath) {
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(getFilesDir() + "/" + zipFilePath);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) {
                    continue;
                }
                String entryName = entry.getName().split("/")[1];
                File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                File entryFile = new File(storageDir, entryName);

                InputStream inputStream = zipFile.getInputStream(entry);

                FileOutputStream outputStream = new FileOutputStream(entryFile);
                byte[] buffer = new byte[1024];
                int bytesRead;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();
                }
            zipFile.close();

            File zipToDelete = new File(getFilesDir() + "/" + zipFilePath);
            zipToDelete.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void openDropboxAuthentication(String dropboxAction){
        try {

            PkceUtils pkce = new PkceUtils();

            String codeVerifier = pkce.generateCodeVerifier();
            SharedPreferences preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("code_verifier", codeVerifier);
            editor.apply();

            String codeChallenge = pkce.generateCodeChallenge(codeVerifier);

            String url = "https://www.dropbox.com/oauth2/authorize?client_id=szhylfeuccf7w84&response_type=code&code_challenge_method=S256&token_access_type=offline&redirect_uri=foodgotten://dropboxauth&code_challenge=" + codeChallenge;

            // Create an Intent to open the browser
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

            // save dropbox action in preferences to retrieve then when returning from browserintent
            // DropboxActivity -> BrowserActivity -> DropboxActivity
            editor.putString(C.DROPBOX_ACTION, dropboxAction);
            editor.apply();

            // Start the browser activity
            startActivityForResult(browserIntent, REQUEST_CODE_BROWSER);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE_BROWSER) {
            if (resultCode == RESULT_OK) {
                String dropboxAction = intent.getStringExtra(C.DROPBOX_ACTION);
                performAction(dropboxAction, intent.getData());
            }
        }
    }

}
