package com.example.foodgotten;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.navigation.NavigationView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FolderSelectorActivity extends AppCompatActivity {

    public ArrayList<String> tickedFolders;
    public ArrayList<String> allFolders;
    public LinearLayout folderlayout;
    public Item item;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.folder_selection);
        this.tickedFolders = new ArrayList<>();
        this.allFolders = Utils.loadFolders(this);
        item = getIntent().getParcelableExtra(C.ITEM);


        folderlayout = findViewById(R.id.folderlistlayout);

        ArrayList<String> folders = Utils.loadFolders(this);

        for(String folder: folders){
            addCheckbox(folder, folderlayout);
        }

        addNewFolderButton();
        addFinishedButton();
    }

    /**
     * Add the ticked folders to the intent and redirect to the addItem / editItem activity
     */
    public void addFinishedButton(){
        ImageButton selectionFinishedButton = findViewById(R.id.selectionFinishedButton);
        selectionFinishedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // return to AddItemView / EditItemView
                Bundle extras = getIntent().getExtras();
                Class destination = AddItemActivity.class;
                if (extras.containsKey(C.INDEX)){
                    destination = EditItemActivity.class;
                }
                Intent myIntent = Utils.createIntentWithItems(getIntent(), FolderSelectorActivity.this, destination);
                item.setFolders(tickedFolders);
                startActivity(myIntent);
            }
        });
    }

    public void addNewFolderButton(){
        ImageButton newFolderButton = findViewById(R.id.addNewFolder);
        newFolderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FolderSelectorActivity.this);
                builder.setTitle("Foldername");

                // Set up the input
                final EditText input = new EditText(FolderSelectorActivity.this);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String foldername = input.getText().toString();
                        addCheckbox(foldername, folderlayout);
                        allFolders.add(foldername);
                        Utils.saveFolders(allFolders, FolderSelectorActivity.this);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    public void addCheckbox(String name, LinearLayout folderlayout){
        final CheckBox folderBox = new CheckBox(this);
        folderBox.setText(name);
        folderlayout.addView(folderBox);

        // Check Boxes if the item is already in the folder
        if(item.getFolders().contains(name)){
            folderBox.setChecked(true);
            tickedFolders.add(name);
        }


        folderBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    tickedFolders.add(folderBox.getText().toString());
                }
                else{
                    tickedFolders.remove(folderBox.getText().toString());
                }

            }
        });
    }
}
